﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CDGShop.Models;

namespace CDGShop.Controllers
{
    public class SinhVienController
    {
        SinhVienModel objSinhVienModel = new SinhVienModel();

        public bool ThemSinhVien(int SinhVienID, string SinhVienName)
        { 
             return objSinhVienModel.ThemSinhVien( SinhVienID, SinhVienName);
        }
    }
}
