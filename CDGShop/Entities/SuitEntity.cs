﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDGShop.Entities
{
    class SuitEntity
    {
        private int _SuitID;

        public int SuitID
        {
            get { return _SuitID; }
            set { _SuitID = value; }
        }
        private float _Suit_Vai;

        public float Suit_Vai
        {
            get { return _Suit_Vai; }
            set { _Suit_Vai = value; }
        }
        private float _Suit_XuoiVai;

        public float Suit_XuoiVai
        {
            get { return _Suit_XuoiVai; }
            set { _Suit_XuoiVai = value; }
        }
        private float _Suit_DaiAo;

        public float DaiAo
        {
            get { return _Suit_DaiAo; }
            set { _Suit_DaiAo = value; }
        }
        private float _Suit_DaiTay;

        public float Suit_DaiTay
        {
            get { return _Suit_DaiTay; }
            set { _Suit_DaiTay = value; }
        }
        private float _Suit_BapTay;

        public float Suit_BapTay
        {
            get { return _Suit_BapTay; }
            set { _Suit_BapTay = value; }
        }
        private float _Suit_VongNach;

        public float Suit_VongNach
        {
            get { return _Suit_VongNach; }
            set { _Suit_VongNach = value; }
        }
        private float _Suit_Co;

        public float Suit_Co
        {
            get { return _Suit_Co; }
            set { _Suit_Co = value; }
        }
        private float _Suit_Nguc;

        public float Suit_Nguc
        {
            get { return _Suit_Nguc; }
            set { _Suit_Nguc = value; }
        }
        private float _Suit_Eo;

        public float Suit_Ngu
        {
            get { return _Suit_Eo; }
            set { _Suit_Eo = value; }
        }
        private float _Suit_Mong;

        public float Suit_Mong
        {
            get { return _Suit_Mong; }
            set { _Suit_Mong = value; }
        }
        private float _Suit_NgucTruoc;

        public float Suit_NgucTruoc
        {
            get { return _Suit_NgucTruoc; }
            set { _Suit_NgucTruoc = value; }
        }
        private float _Suit_NgucSau;

        public float Suit_NgucSau
        {
            get { return _Suit_NgucSau; }
            set { _Suit_NgucSau = value; }
        }
    }
}
