﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDGShop.Entities
{
    class FabricEntity
    {
        private int _FabricID;

        public int FabricID
        {
            get { return _FabricID; }
            set { _FabricID = value; }
        }

        private float _Fabric_Name;

        public float Fabric_Name
        {
            get { return _Fabric_Name; }
            set { _Fabric_Name = value; }
        }
        private float _Images;

        public float Images
        {
            get { return _Images; }
            set { _Images = value; }
        }
    }
}
