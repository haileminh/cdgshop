﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDGShop.Entities
{
    class QuanEntity
    {
        private int _QuanID;
        public int QuanID
        {
         get { return _QuanID; }
         set { _QuanID = value; }
        }
        private float _QuanDai; 
    

        public float QuanDai
        {
            get { return _QuanDai; }
            set { _QuanDai = value; }
        }
    
        private float _QuanBung;
        public float QuanBung
        {
            get { return _QuanBung; }
            set { _QuanBung = value; }
        }
    
        private float _QuanGoi;
        public float QuanGoi
        {
            get { return _QuanGoi; }
            set { _QuanGoi = value; }
        }
        private float _QuanMong;

        public float QuanMong
        {
            get { return _QuanMong; }
            set { _QuanMong = value; }
        }
        private float _QuanDui;
        public float QuanDui
        {
            get { return _QuanDui; }
            set { _QuanDui = value; }
        }
        private float _QuanOng;
        public float QuanOng
        {
            get { return _QuanOng; }
            set { _QuanOng = value; }
        }
        private float _QuanDay;
        public float QuanDay
        {
            get { return _QuanDay; }
            set { _QuanDay = value; }
        }
    }
}
