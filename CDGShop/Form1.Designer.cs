﻿namespace CDGShop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.backstageViewControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewControl();
            this.backstageViewClientControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl2 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl3 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl4 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl5 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl6 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewClientControl7 = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.backstageViewTabItem1 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewItemSeparator1 = new DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator();
            this.backstageViewTabItem2 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewItemSeparator2 = new DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator();
            this.backstageViewTabItem3 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewTabItem4 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewItemSeparator3 = new DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator();
            this.backstageViewTabItem5 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewItemSeparator4 = new DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator();
            this.backstageViewTabItem6 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.backstageViewTabItem7 = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.backstageViewControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonDropDownControl = this.backstageViewControl1;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 4;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(891, 144);
            // 
            // backstageViewControl1
            // 
            this.backstageViewControl1.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Yellow;
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl1);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl2);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl3);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl4);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl5);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl6);
            this.backstageViewControl1.Controls.Add(this.backstageViewClientControl7);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem1);
            this.backstageViewControl1.Items.Add(this.backstageViewItemSeparator1);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem2);
            this.backstageViewControl1.Items.Add(this.backstageViewItemSeparator2);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem3);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem4);
            this.backstageViewControl1.Items.Add(this.backstageViewItemSeparator3);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem5);
            this.backstageViewControl1.Items.Add(this.backstageViewItemSeparator4);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem6);
            this.backstageViewControl1.Items.Add(this.backstageViewTabItem7);
            this.backstageViewControl1.Location = new System.Drawing.Point(72, 146);
            this.backstageViewControl1.Name = "backstageViewControl1";
            this.backstageViewControl1.Ribbon = this.ribbonControl1;
            this.backstageViewControl1.SelectedTab = this.backstageViewTabItem1;
            this.backstageViewControl1.SelectedTabIndex = 0;
            this.backstageViewControl1.Size = new System.Drawing.Size(487, 378);
            this.backstageViewControl1.TabIndex = 1;
            // 
            // backstageViewClientControl1
            // 
            this.backstageViewClientControl1.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl1.Name = "backstageViewClientControl1";
            this.backstageViewClientControl1.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl1.TabIndex = 0;
            // 
            // backstageViewClientControl2
            // 
            this.backstageViewClientControl2.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl2.Name = "backstageViewClientControl2";
            this.backstageViewClientControl2.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl2.TabIndex = 1;
            // 
            // backstageViewClientControl3
            // 
            this.backstageViewClientControl3.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl3.Name = "backstageViewClientControl3";
            this.backstageViewClientControl3.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl3.TabIndex = 2;
            // 
            // backstageViewClientControl4
            // 
            this.backstageViewClientControl4.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl4.Name = "backstageViewClientControl4";
            this.backstageViewClientControl4.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl4.TabIndex = 3;
            // 
            // backstageViewClientControl5
            // 
            this.backstageViewClientControl5.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl5.Name = "backstageViewClientControl5";
            this.backstageViewClientControl5.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl5.TabIndex = 4;
            // 
            // backstageViewClientControl6
            // 
            this.backstageViewClientControl6.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl6.Name = "backstageViewClientControl6";
            this.backstageViewClientControl6.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl6.TabIndex = 5;
            // 
            // backstageViewClientControl7
            // 
            this.backstageViewClientControl7.Location = new System.Drawing.Point(132, 0);
            this.backstageViewClientControl7.Name = "backstageViewClientControl7";
            this.backstageViewClientControl7.Size = new System.Drawing.Size(355, 378);
            this.backstageViewClientControl7.TabIndex = 6;
            // 
            // backstageViewTabItem1
            // 
            this.backstageViewTabItem1.Appearance.BackColor = System.Drawing.Color.White;
            this.backstageViewTabItem1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.backstageViewTabItem1.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem1.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem1.Appearance.Options.UseBackColor = true;
            this.backstageViewTabItem1.Appearance.Options.UseFont = true;
            this.backstageViewTabItem1.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem1.Caption = "Info";
            this.backstageViewTabItem1.ContentControl = this.backstageViewClientControl1;
            this.backstageViewTabItem1.Name = "backstageViewTabItem1";
            this.backstageViewTabItem1.Selected = true;
            // 
            // backstageViewItemSeparator1
            // 
            this.backstageViewItemSeparator1.Name = "backstageViewItemSeparator1";
            // 
            // backstageViewTabItem2
            // 
            this.backstageViewTabItem2.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem2.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem2.Appearance.Options.UseFont = true;
            this.backstageViewTabItem2.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem2.Caption = "Open";
            this.backstageViewTabItem2.ContentControl = this.backstageViewClientControl2;
            this.backstageViewTabItem2.Name = "backstageViewTabItem2";
            this.backstageViewTabItem2.Selected = false;
            // 
            // backstageViewItemSeparator2
            // 
            this.backstageViewItemSeparator2.Name = "backstageViewItemSeparator2";
            // 
            // backstageViewTabItem3
            // 
            this.backstageViewTabItem3.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem3.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem3.Appearance.Options.UseFont = true;
            this.backstageViewTabItem3.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem3.Caption = "Import";
            this.backstageViewTabItem3.ContentControl = this.backstageViewClientControl3;
            this.backstageViewTabItem3.Name = "backstageViewTabItem3";
            this.backstageViewTabItem3.Selected = false;
            // 
            // backstageViewTabItem4
            // 
            this.backstageViewTabItem4.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem4.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem4.Appearance.Options.UseFont = true;
            this.backstageViewTabItem4.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem4.Caption = "Export";
            this.backstageViewTabItem4.ContentControl = this.backstageViewClientControl4;
            this.backstageViewTabItem4.Name = "backstageViewTabItem4";
            this.backstageViewTabItem4.Selected = false;
            // 
            // backstageViewItemSeparator3
            // 
            this.backstageViewItemSeparator3.Name = "backstageViewItemSeparator3";
            // 
            // backstageViewTabItem5
            // 
            this.backstageViewTabItem5.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem5.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem5.Appearance.Options.UseFont = true;
            this.backstageViewTabItem5.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem5.Caption = "Close";
            this.backstageViewTabItem5.ContentControl = this.backstageViewClientControl5;
            this.backstageViewTabItem5.Name = "backstageViewTabItem5";
            this.backstageViewTabItem5.Selected = false;
            // 
            // backstageViewItemSeparator4
            // 
            this.backstageViewItemSeparator4.Name = "backstageViewItemSeparator4";
            // 
            // backstageViewTabItem6
            // 
            this.backstageViewTabItem6.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem6.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem6.Appearance.Options.UseFont = true;
            this.backstageViewTabItem6.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem6.Caption = "Account";
            this.backstageViewTabItem6.ContentControl = this.backstageViewClientControl6;
            this.backstageViewTabItem6.Name = "backstageViewTabItem6";
            this.backstageViewTabItem6.Selected = false;
            // 
            // backstageViewTabItem7
            // 
            this.backstageViewTabItem7.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.backstageViewTabItem7.Appearance.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.backstageViewTabItem7.Appearance.Options.UseFont = true;
            this.backstageViewTabItem7.Appearance.Options.UseForeColor = true;
            this.backstageViewTabItem7.Caption = "Options";
            this.backstageViewTabItem7.ContentControl = this.backstageViewClientControl7;
            this.backstageViewTabItem7.Name = "backstageViewTabItem7";
            this.backstageViewTabItem7.Selected = false;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Đăng nhập";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Thoát";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Đổi tài khoản";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Appearance.ForeColor = System.Drawing.Color.Teal;
            this.ribbonPage1.Appearance.Options.UseForeColor = true;
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "HỆ THỐNG";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Quản trị hệ thống";
            // 
            // Form1
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 536);
            this.Controls.Add(this.backstageViewControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl1;
            this.Text = "PHẦN MỀM QUẢN LÝ CỬA HIỆU MAY QUẦN ÁO ICLASSIS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.backstageViewControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.BackstageViewControl backstageViewControl1;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl1;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem1;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl2;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl3;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl4;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl5;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl6;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl backstageViewClientControl7;
        private DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator backstageViewItemSeparator1;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem2;
        private DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator backstageViewItemSeparator2;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem3;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem4;
        private DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator backstageViewItemSeparator3;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem5;
        private DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator backstageViewItemSeparator4;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem6;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem backstageViewTabItem7;
    }
}

