﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CDGShop.Views
{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có chắc chắn muốn thoát khỏi phần mềm", "Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.Close();
            }
            else { }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text =="" || txtPassword.Text=="")
            {
                MessageBox.Show("Bạn chưa nhập vào tài khoản hoặc mật khẩu","Thông báo",MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}