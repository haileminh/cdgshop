﻿namespace CDGShop
{
    partial class Demo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSinhVienID = new DevExpress.XtraEditors.TextEdit();
            this.btnThemSinhVien = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtSinhVienName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSinhVienID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSinhVienName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSinhVienID
            // 
            this.txtSinhVienID.Location = new System.Drawing.Point(134, 45);
            this.txtSinhVienID.Name = "txtSinhVienID";
            this.txtSinhVienID.Size = new System.Drawing.Size(227, 20);
            this.txtSinhVienID.TabIndex = 0;
            // 
            // btnThemSinhVien
            // 
            this.btnThemSinhVien.Location = new System.Drawing.Point(134, 143);
            this.btnThemSinhVien.Name = "btnThemSinhVien";
            this.btnThemSinhVien.Size = new System.Drawing.Size(142, 35);
            this.btnThemSinhVien.TabIndex = 1;
            this.btnThemSinhVien.Text = "THÊM SINH VIÊN";
            this.btnThemSinhVien.Click += new System.EventHandler(this.btnThemSinhVien_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(24, 48);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(59, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Mã sinh viên";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(24, 97);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(63, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Tên sinh viên";
            // 
            // txtSinhVienName
            // 
            this.txtSinhVienName.Location = new System.Drawing.Point(134, 94);
            this.txtSinhVienName.Name = "txtSinhVienName";
            this.txtSinhVienName.Size = new System.Drawing.Size(227, 20);
            this.txtSinhVienName.TabIndex = 0;
            // 
            // Demo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 248);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnThemSinhVien);
            this.Controls.Add(this.txtSinhVienName);
            this.Controls.Add(this.txtSinhVienID);
            this.Name = "Demo";
            this.Text = "Demo";
            ((System.ComponentModel.ISupportInitialize)(this.txtSinhVienID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSinhVienName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtSinhVienID;
        private DevExpress.XtraEditors.SimpleButton btnThemSinhVien;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtSinhVienName;
    }
}